<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sepatu extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model("sepatu_model");
        $this->load->library('form_validation');
    }

	public function index()
	{
		$data['sepatu'] = $this->sepatu_model->getAll();
		$this->load->view('sepatu/header');
		$this->load->view('sepatu/index',$data);
		$this->load->view('sepatu/footer');
	}

	public function create()
	{
		$this->load->view('sepatu/header');
		$this->load->view('sepatu/create');
		$this->load->view('sepatu/footer');
	}

	public function save()
	{
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('no_telp','Nomor Telepon','required');
		$this->form_validation->set_rules('id_sepatu','Jenis Sepatu','required');
		$this->form_validation->set_rules('ukuran_sepatu','Ukuran Sepatu','required');
		if ($this->form_validation->run()==true)
        {
			$data['nama'] = $this->input->post('nama');
			$data['id_sepatu'] = $this->input->post('id_sepatu');
			$data['ukuran_sepatu'] = $this->input->post('ukuran_sepatu');
			$data['no_telp'] = $this->input->post('no_telp');
			$this->sepatu_model->save($data);
			redirect('sepatu');
		}
		else
		{
			$this->load->view('sepatu/header');
			$this->load->view('sepatu/create');
			$this->load->view('sepatu/footer');
		}
	}
}
