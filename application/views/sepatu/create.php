  <div class="container">
		<div class="card">
			<div class="card-header">Create Pembelian Sepatu</div>
			<div class="card-body">
			<?php 
			if(validation_errors() != false)
			{
				?>
				<div class="alert alert-danger" role="alert">
					<?php echo validation_errors(); ?>
				</div>
				<?php
			}
			?>
			<form method="post" action="<?php echo base_url(); ?>sepatu/save">
				<div class="form-group">
					<label for="nama">Nama</label>
					<input type="text" class="form-control" id="nama" name="nama">
				</div>
				
				<div class="form-group">
					<label for="no_telp">No Telp</label>
					<input type="number" class="form-control" id="no_telp" name="no_telp">
				</div>

				<div class="form-group">
					<label for="id_sepatu">Jenis Sepatu</label>
					<select name="id_sepatu" id="id_sepatu" class="form-control">
						<option value="1">Nike</option>
						<option value="2">Adidas</option>
						<option value="3">Kickers</option>
						<option value="4">Eiger</option>
						<option value="5">Bucherri</option>
					</select>
				</div>

				<div class="form-group">
					<label for="ukuran_sepatu">Ukuran Sepatu</label>
					<select name="ukuran_sepatu" class="form-control" id="ukuran_sepatu">
					<option value="32">32</option>
						<option value="33">33</option>
						<option value="34">34</option>
						<option value="35">35</option>
						<option value="36">36</option>
						<option value="37">37</option>
						<option value="38">38</option>
						<option value="39">39</option>
						<option value="40">40</option>
						<option value="41">41</option>
						<option value="42">42</option>
						<option value="43">43</option>
						<option value="44">44</option>
					</select>
				</div>

				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
			</div>
		</div>
	</div>


