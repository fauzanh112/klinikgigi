<main role="main" class="container">
		<div class="card">
			<div class="card-header">Data Penjualan Sepatu</div>
			<div class="card-body">
				<a href="<?php echo base_url(); ?>sepatu/create" class="btn btn-success">Create</a>
				<br/>
				<br/>
				<table class="table table-bordered">
					<tr>
						<th width="5%">No</th>
						<th>Nama</th>
						<th>Telp</th>
						<th>Sepatu</th>
						<th>Ukuran</th>
						<th>Harga</th>
					</tr>
					<?php 
					$no = 1;
					foreach($sepatu as $row)
					{
						?>
						<tr>
							<td widtd="5%"><?php echo $no++; ?></td>
							<td><?php echo $row->nama; ?></td>
							<td><?php echo $row->no_telp; ?></td>
							<td><?php echo $row->jenis_sepatu; ?></td>
							<td><?php echo $row->ukuran_sepatu; ?></td>
							<td><?php echo $row->harga; ?></td>
						</tr>
						<?php
					}
					?>
				</table>
			</div>
		</div>
</main>
