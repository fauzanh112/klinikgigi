  <div class="container">
		<div class="card">
			<div class="card-header">Edit Siswa</div>
			<div class="card-body">
			<?php 
			if(validation_errors() != false)
			{
				?>
				<div class="alert alert-danger" role="alert">
					<?php echo validation_errors(); ?>
				</div>
				<?php
			}
			?>
			<form method="post" action="<?php echo base_url(); ?>sepatu/update">
				<input type="hidden" name="id_pelanggan" id="id_pelanggan" value="<?php echo $sepatu->id_pelanggan; ?>"/>
				<div class="form-group">
					<label for="nama">Nama</label>
					<input type="text" value="<?php echo $sepatu->nama; ?>" class="form-control" id="nama" name="nama">
				</div>

				<div class="form-group">
					<label for="jenis_sepatu">Jenis Kelamin</label>
					<select name="jenis_sepatu" id="jenis_sepatu" class="form-control">
						<option value="pria" <?php echo ($sepatu->jenis_sepatu ? 'pria' : 'selected' ); ?> >Pria</option>
						<option value="wanita" <?php echo ($sepatu->jenis_sepatu ? 'wanita' : 'selected' ); ?>>Wanita</option>
					</select>
				</div>

				<div class="form-group">
					<label for="ukuran_sepatu">Tempat Lahir</label>
					<input type="text" class="form-control" id="ukuran_sepatu" name="ukuran_sepatu" value="<?php echo $sepatu->ukuran_sepatu; ?>">
				</div>

				<div class="form-group">
					<label for="tgl_lahir">Tanggal Lahir</label>
					<input type="text" class="form-control datepicker"  readonly id="tanggal_lahir" name="tanggal_lahir" value="<?php echo $sepatu->tanggal_lahir; ?>">
				</div>

				<div class="form-group">
					<label for="no_telp">No Telp</label>
					<input type="number" class="form-control" value="<?php echo $sepatu->no_telp; ?>" id="no_telp" name="no_telp">
				</div>

				<div class="form-group">
					<label for="alamat">Alamat</label>
					<textarea class="form-control" name="alamat" id="alamat"><?php echo $sepatu->alamat; ?></textarea>
				</div>

				<button type="submit" class="btn btn-primary">Update</button>
			</form>
			</div>
		</div>
	</div>

