<?php defined('BASEPATH') OR exit('No direct script access allowed');

class sepatu_model extends CI_Model
{

    public function getAll()
    {
        return $this->db->FROM ('tb_pelanggan')->join('tb_harga','tb_harga.id_sepatu = tb_pelanggan.id_sepatu')->get()->result();
	}
	
	public function save($data)
    {
        return $this->db->insert('tb_pelanggan',$data);
    }
}
